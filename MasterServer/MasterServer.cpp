// MasterServer.cpp : Defines the entry point for the console application.
//

#include <iostream>

#include <io.h>
#include <sys/types.h>
#include <sys/stat.h> 

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <ctime>

#include <map>
#include <deque>

#include <windows.h>

#include "curl/curl.h"

#include "config.h"

#include "ServerInstance.h"

#include "stdafx.h"
#include "mongoose.h"

#include "zip.h"

#include "rapidjson/prettywriter.h" // for stringify JSON

using namespace rapidjson;
using namespace std;

enum RESPONSE_CODE
{
	SUCCESS = 200,

	OUT_OF_PORT = 10,
	OUT_OF_INSTANCE = 11,
	CREATE_PROCESS_FAILED = 12,
	TERMINATE_PROCESS_FAILED = 13,

	INVALID_PARAM = 100,
};

struct cmp_str
{
   bool operator()(char const *a, char const *b) const
   {
      return std::strcmp(a, b) < 0;
   }
};

static const char *s_no_cache_header =
  "Cache-Control: max-age=0, post-check=0, "
  "pre-check=0, no-store, no-cache, must-revalidate\r\n";

void OnCreateGameServer(ServerInstance* instance);
void OnDestroyGameServer(ServerInstance* instance);

bool quit;

char buffer[100];

map<char*, ServerInstance*, cmp_str> serverInstances;
deque<int> portPool;

int portMin;
int portMax;

int maxInstance;

string execPath;
string execName;

string workingPath;

string cmdArgs;

string logPath;
string logUploadUrl;

static void handle_restful_call(struct mg_connection *conn)
{
	// Get form variables
	mg_get_var(conn, "n1", buffer, sizeof(buffer));
	int n1 = strtod(buffer, NULL);

	mg_get_var(conn, "n2", buffer, sizeof(buffer));
	int n2 = strtod(buffer, NULL);

	mg_printf_data(conn, "{ \"result\": %lf }",  n1 + n2);
}

int GetPort()
{
	if (portPool.empty()) return -1;

	int ret = portPool.front();
	portPool.pop_front();

	return ret;
}

void RecyclePort(int port)
{
	portPool.push_back(port);
}

void FindAndReplaceString(string& str, const string& oldStr, const string& newStr)
{
	size_t pos = 0;
	while ((pos = str.find(oldStr, pos)) != string::npos)
	{
		str.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}
}

BOOL DeleteDirectory(char* sPath) 
{
    HANDLE hFind;  // file handle
    WIN32_FIND_DATAA FindFileData;

    char DirPath[MAX_PATH];
    char FileName[MAX_PATH];

	strcpy_s(DirPath, sPath);
	strcat_s(DirPath, "\\*");  // searching all files

	strcpy_s(FileName, sPath);
	strcat_s(FileName, "\\");

    hFind = FindFirstFileA(DirPath, &FindFileData); // find the first file
    if (hFind == INVALID_HANDLE_VALUE) return FALSE;
    strcpy_s(DirPath, FileName);

    bool bSearch = true;
    while (bSearch) // until we finds an entry
	{ 
        if (FindNextFileA(hFind, &FindFileData)) 
		{
			char* fileName = FindFileData.cFileName;
			if (fileName != NULL)
			{
				if (strlen(fileName) == 1 && fileName[0] == '.') continue;
				if (strlen(fileName) == 2 && fileName[0] == '.' && fileName[1] == '.') continue;
			}

            strcat_s(FileName, fileName);

            if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) 
			{
                // we have found a directory, recurse
                if (!DeleteDirectory(FileName)) 
				{ 
                    FindClose(hFind); 
                    return FALSE; // directory couldn't be deleted
                }

                RemoveDirectoryA(FileName); // remove the empty directory
                strcpy_s(FileName, DirPath);
            }
            else 
			{
                if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
                    _chmod(FileName, _S_IWRITE); // change read-only file mode
				}

                if (!DeleteFileA(FileName))  // delete the file
				{ 
                    FindClose(hFind); 
                    return FALSE; 
                }                 

                strcpy_s(FileName, DirPath);
            }
        }
        else 
		{
            if(GetLastError() == ERROR_NO_MORE_FILES) // no more files there
			{
				bSearch = false;
			}
            else 
			{
                // some error occured, close the handle and return FALSE
                FindClose(hFind); 
                return FALSE;
            }
        }
    }

    FindClose(hFind);  // closing file handle
    return RemoveDirectoryA(sPath); // remove the empty directory
}

void ZipDirectory(char* sPath) 
{
    HANDLE hFind;  // file handle
    WIN32_FIND_DATAA FindFileData;

	string zipPath = sPath;
	zipPath += ".zip";

	HZIP hz = CreateZip(zipPath.c_str(), NULL); 

    char DirPath[MAX_PATH];
    char FileName[MAX_PATH];

	strcpy_s(DirPath, sPath);
	strcat_s(DirPath, "\\*");  // searching all files

	strcpy_s(FileName, sPath);
	strcat_s(FileName, "\\");

	hFind = FindFirstFileA(DirPath, &FindFileData); // find the first file
    if (hFind == INVALID_HANDLE_VALUE) 
	{
		 CloseZip(hz);
		 return;
	}

    strcpy_s(DirPath, FileName);

    bool bSearch = true;
    while (bSearch) // until we finds an entry
	{ 
        if (FindNextFileA(hFind, &FindFileData)) 
		{
			char* fileName = FindFileData.cFileName;

			if (fileName != NULL)
			{
				if (strlen(fileName) == 1 && fileName[0] == '.') continue;
				if (strlen(fileName) == 2 && fileName[0] == '.' && fileName[1] == '.') continue;
			}

            if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
                continue;
            }
            else 
			{
				strcat_s(FileName, fileName);

				ZipAdd(hz, fileName, FileName);
				
                if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
				{
                    _chmod(FileName, _S_IWRITE); // change read-only file mode
				}

				strcpy_s(FileName, DirPath);
            }
        }
        else 
		{
            if(GetLastError() == ERROR_NO_MORE_FILES) // no more files there
			{
				bSearch = false;
			}
            else 
			{
                // some error occured, close the handle and return FALSE
                FindClose(hFind); 

				CloseZip(hz);
                return;
            }
        }
    }

    FindClose(hFind);  // closing file handle
	CloseZip(hz);
}

void UploadFile(char* filePath)
{
	double speed_upload, total_time;
	FILE *fd;
 
	fopen_s(&fd, filePath, "rb"); /* open file to upload */ 
	if (!fd) return;

	struct stat file_info;
	if (fstat(_fileno(fd), &file_info) != 0) return;
	
	CURL *curl;
	CURLcode res;

	curl = curl_easy_init();
	if(curl) 
	{
		/* upload to this place */ 
		curl_easy_setopt(curl, CURLOPT_URL, logUploadUrl.c_str());
 
		/* tell it to "upload" to the URL */ 
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
 
		/* set where to read from (on Windows you need to use READFUNCTION too) */ 
		curl_easy_setopt(curl, CURLOPT_READDATA, fd);
 
		/* and give the size of the upload (optional) */ 
		curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)file_info.st_size);
 
		/* enable verbose for easier tracing */ 
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK) 
		{
		  fprintf(stderr, "curl_easy_perform() failed: %s\n",
				  curl_easy_strerror(res));
 
		}
		else 
		{
		  /* now extract transfer info */ 
		  curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed_upload);
		  curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total_time);
 
		  fprintf(stderr, "Speed: %.3f bytes/sec during %.3f seconds\n",
				  speed_upload, total_time);
 
		}

		/* always cleanup */ 
		curl_easy_cleanup(curl);
	}
}

char* CreateGameId()
{
	time_t now = time(0);

	tm ltm;
	localtime_s(&ltm, &now);

	int noise = rand() % 10000;

	sprintf_s(buffer, 100, "%03d%02d%02d%02d%04d", ltm.tm_yday, ltm.tm_hour, ltm.tm_min, ltm.tm_sec, noise);

	return _strdup(buffer);
}

static void CreateServer(struct mg_connection *conn)
{
	STARTUPINFOA si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    ZeroMemory( &pi, sizeof(pi) );

    si.cb = sizeof(si);

	StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);

	writer.StartObject();

	int numInstances = serverInstances.size();
	if (numInstances >= maxInstance)
	{
		writer.String("code");
		writer.Int(RESPONSE_CODE::OUT_OF_INSTANCE);
	}
	else 
	{
		int port = GetPort();

		if (port == -1)
		{
			writer.String("code");
			writer.Int(RESPONSE_CODE::OUT_OF_PORT);
		}
		else 
		{
			char* gameId = CreateGameId();

			string path = execPath + "\\" + execName;

			string args = " " + cmdArgs;
			FindAndReplaceString(args, "<game_id>", gameId);
			FindAndReplaceString(args, "<server_port>", to_string(port));

			string instanceLogPath = logPath + "\\" + gameId;
			FindAndReplaceString(args, "<log_path>", instanceLogPath);

			CreateDirectoryA(instanceLogPath.c_str(), NULL);
					
			if (!CreateProcessA(path.c_str(), (char*)args.c_str(), NULL, NULL,FALSE, CREATE_NEW_CONSOLE, NULL, workingPath.c_str(), &si, &pi)) 
			{
				RecyclePort(port);

				writer.String("code");
				writer.Int(RESPONSE_CODE::CREATE_PROCESS_FAILED);
			}
			else
			{
				int processId = pi.dwProcessId;
				
				ServerInstance* serverInstance = new ServerInstance(gameId, pi.hProcess, pi.hThread, port);
		
				OnCreateGameServer(serverInstance);

				serverInstances[gameId] = serverInstance;
		
				writer.String("code");
				writer.Int(RESPONSE_CODE::SUCCESS);

				writer.String("id");
				writer.String(gameId);

				writer.String("port");
				writer.Int(port);
			}
		}
	}

	writer.EndObject();

	mg_printf_data(conn, sb.GetString());
}

static void DestroyServer(struct mg_connection *conn)
{
	mg_get_var(conn, "id", buffer, sizeof(buffer));

	StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);

	writer.StartObject();

	map<char*, ServerInstance*>::iterator iter = serverInstances.find(buffer);
	if (iter != serverInstances.end())
	{
		ServerInstance* serverInstance = iter->second;
	
		if (TerminateProcess(serverInstance->GetProcessHandle(), 100))
		{
			writer.String("code");
			writer.Int(RESPONSE_CODE::SUCCESS);
		}
		else 
		{
			writer.String("code");
			writer.Int(RESPONSE_CODE::TERMINATE_PROCESS_FAILED);
		}

		OnDestroyGameServer(serverInstance);

		delete serverInstance;
		serverInstances.erase(iter);
	}
	else 
	{
		writer.String("code");
		writer.Int(RESPONSE_CODE::INVALID_PARAM);
	}

	writer.EndObject();

	mg_printf_data(conn, sb.GetString());
}

static void DestroyAllServer(struct mg_connection *conn)
{
	StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);

	writer.StartObject();

	bool success = true;

	for (map<char*, ServerInstance*>::iterator iter = serverInstances.begin(); iter != serverInstances.end(); ++iter)
	{
		ServerInstance* serverInstance = iter->second;
	
		if (!TerminateProcess(serverInstance->GetProcessHandle(), 100)) success = false;

		OnDestroyGameServer(serverInstance);
		delete serverInstance;
	}
	
	writer.String("code");
	if (success) writer.Int(RESPONSE_CODE::SUCCESS);
	else writer.Int(RESPONSE_CODE::TERMINATE_PROCESS_FAILED);
	
	writer.EndObject();

	mg_printf_data(conn, sb.GetString());
}

static void Terminate(struct mg_connection *conn)
{
	quit = true;
	mg_printf_data(conn, "");
}

static void CountServer(struct mg_connection *conn)
{
	StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);

	writer.StartObject();

	writer.String("count");
	writer.Int(serverInstances.size());

	writer.EndObject();

	mg_printf_data(conn, sb.GetString());
}

static void CountAvailableServer(struct mg_connection *conn)
{
	StringBuffer sb;
    PrettyWriter<StringBuffer> writer(sb);

	writer.StartObject();

	writer.String("count");
	writer.Int(max(maxInstance - serverInstances.size(), 0));

	writer.EndObject();

	mg_printf_data(conn, sb.GetString());
}


void OnCreateGameServer(ServerInstance* instance)
{
	printf("Create Game Server\n");
}

void OnDestroyGameServer(ServerInstance* instance)
{
	printf("Destroy Game Server\n");

	char* gameId = instance->GetId();

	if (logPath.length() > 0)
	{
		string instanceLogPath = logPath + "\\" + gameId;

		ZipDirectory((char*)instanceLogPath.c_str());

		if (logUploadUrl.length() > 0)
		{
			string zipLogPath = instanceLogPath + ".zip";
			UploadFile((char*)zipLogPath.c_str());

			DeleteFileA(zipLogPath.c_str());
		}

		DeleteDirectory((char*)instanceLogPath.c_str());
	}

	free(gameId);

	RecyclePort(instance->GetPort());

	CloseHandle(instance->GetProcessHandle());
	CloseHandle(instance->GetThreadHandle());
}

static int ev_handler(struct mg_connection *conn, enum mg_event ev)
{
  switch (ev) 
  {
    case MG_AUTH: return MG_TRUE;
    case MG_REQUEST:

      if (!strcmp(conn->uri, "/api/sum")) 
	  {
        handle_restful_call(conn);
        return MG_TRUE;
      }

	  if (!strcmp(conn->uri, "/create_server")) 
	  {
        CreateServer(conn);
        return MG_TRUE;
      }

	  if (!strcmp(conn->uri, "/destroy_server")) 
	  {
        DestroyServer(conn);
        return MG_TRUE;
      }

	  if (!strcmp(conn->uri, "/destroy_all_server")) 
	  {
        DestroyAllServer(conn);
        return MG_TRUE;
      }

	  if (!strcmp(conn->uri, "/terminate")) 
	  {
        Terminate(conn);
        return MG_TRUE;
      }

	  if (!strcmp(conn->uri, "/count_server")) 
	  {
        CountServer(conn);
        return MG_TRUE;
      }

	  if (!strcmp(conn->uri, "/count_available_instance")) 
	  {
        CountAvailableServer(conn);
        return MG_TRUE;
      }

      mg_send_file(conn, "index.html", s_no_cache_header);
      return MG_MORE;
    default: return MG_FALSE;
  }
}

void Update()
{
	const double UPDATE_TIME = 5.0; //s

	static clock_t lastUpdateTime = 0;
		
	clock_t currentTime = clock();
	if (currentTime - lastUpdateTime < UPDATE_TIME * CLOCKS_PER_SEC) return;

	lastUpdateTime = currentTime;

	DWORD ret;

	for (map<char*, ServerInstance*>::iterator iter = serverInstances.begin(); iter != serverInstances.end();)
	{
		ServerInstance* serverInstance = iter->second;
		BOOL succeeded = GetExitCodeProcess(serverInstance->GetProcessHandle(), &ret);

		if (!succeeded || ret != STILL_ACTIVE)
		{
			OnDestroyGameServer(serverInstance);

			delete serverInstance;
			iter = serverInstances.erase(iter);
		}
		else 
		{
			++iter;
		}
	}
}

int main(int argc, char* argv[], char* envp[])
{
	quit = false;

	srand(time(NULL));

	Config config("config.cfg", envp);

	maxInstance = config.pInt("max_instance");

	portMin = config.pInt("port_min");
	portMax = config.pInt("port_max");

	execPath = config.pString("exec_path");
	execName = config.pString("exec_name");

	workingPath = config.pString("working_path");

	cmdArgs = config.pString("cmd_args");

	logPath = config.pString("log_path");
	logUploadUrl = config.pString("log_upload_url");

	for (int i = portMin; i <= portMax; ++i) portPool.push_back(i);
	
	int numPorts = portPool.size();
	for (int i = numPorts - 1; i > 0; --i)
	{
		int j = rand() % (i + 1);

		int tmp = portPool[i];
		portPool[i] = portPool[j];
		portPool[j] = tmp;
	}

	if (logPath.length() > 0)
	{
		CreateDirectoryA(logPath.c_str(), NULL);
	}
	
	struct mg_server *server;

	// Create and configure the server
	server = mg_create_server(NULL, ev_handler);
	mg_set_option(server, "listening_port", "8000");

	// Serve request. Hit Ctrl-C to terminate the program
	printf("Starting on port %s\n", mg_get_option(server, "listening_port"));

	while (!quit)
	{
		mg_poll_server(server, 1000);
		Update();
	}

	for (map<char*, ServerInstance*>::iterator iter = serverInstances.begin(); iter != serverInstances.end(); ++iter)
	{
		ServerInstance* serverInstance = iter->second;
	
		TerminateProcess(serverInstance->GetProcessHandle(), 100);

		OnDestroyGameServer(serverInstance);
		delete serverInstance;
	}

	// Cleanup, and free server instance
	mg_destroy_server(&server);

	return 0;
}

