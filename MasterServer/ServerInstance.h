#include <windows.h>

class ServerInstance
{
public :
	ServerInstance(char* id, HANDLE processHandle, HANDLE threadHandle, int port);
	~ServerInstance();

	char* GetId();

	HANDLE GetProcessHandle();
	HANDLE GetThreadHandle();

	int GetPort();

private:
	HANDLE processHandle;
	HANDLE threadHandle;

	char* id;

	int port;
};