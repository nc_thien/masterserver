#include "ServerInstance.h"

ServerInstance::ServerInstance(char* id, HANDLE processHandle, HANDLE threadHandle, int port)
{
	this->id = id;

	this->processHandle = processHandle;
	this->threadHandle = threadHandle;

	this->port = port;
}

ServerInstance::~ServerInstance()
{
}

char* ServerInstance::GetId()
{
	return id;
}

HANDLE ServerInstance::GetProcessHandle()
{
	return processHandle;
}

HANDLE ServerInstance::GetThreadHandle()
{
	return threadHandle;
}

int ServerInstance::GetPort()
{
	return port;
}